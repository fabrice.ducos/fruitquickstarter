module myreader_module
  implicit none
  private

  type, public :: myreader
     logical :: is_valid
     character(512) :: input_file
  end type myreader

  public :: myreader_new

  contains

    function myreader_new(input_file) result(object)
      character(*), intent(in) :: input_file
      type(myreader) :: object

      object%is_valid = .false.
      object%input_file = input_file

      if (file_exists(input_file)) then
        object%is_valid = .true.
      end if

    end function myreader_new

  function file_exists(filename) result(res)
    implicit none
    character(len=*),intent(in) :: filename
    logical                     :: res

    ! Check if the file exists
    inquire( file=trim(filename), exist=res )
  end function

end module myreader_module
