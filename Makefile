include rubylib.cfg

FC=gfortran
FFLAGS=-Wall -Wextra -pedantic -fbounds-check -Wuninitialized -O -g \
 -Wno-unused-parameter -cpp

fruit_code = fruit/fruit.f90
code = myreader_module.f90 myreader_test.f90
code_gen = fruit_basket_gen.f90 fruit_driver_gen.f90

all_code = $(fruit_code) $(code) $(code_gen)

driver = fruit_driver.exe

all : build test

build : $(driver)

fruit_basket_gen.f90 : $(code)
	ruby fruit_generator.rb

fruit_driver_gen.f90 : $(code)
	ruby fruit_generator.rb

fruit_driver.exe : $(all_code)
	$(FC) $(FFLAGS) $(all_code) -o fruit_driver.exe

clean : 
	rm -f *.o *.mod *.obj *.exe
	rm -rf *.dSYM
	rm -f fruit_driver_gen.f90
	rm -f fruit_basket_gen.f90
	rm -f result.xml result_tmp.xml

test : $(driver)
	./$(driver)

.PHONY : all clean test

