module myreader_test
  use fruit
  use myreader_module
  implicit none

  character(*), parameter :: DATA_ROOT = 'data'

contains

  subroutine test_myreader_new
    type(myreader) :: reader
    character(*), parameter :: input_file = DATA_ROOT // '/some_file.txt'

    reader = myreader_new(input_file)
    call assert_equals(input_file, reader%input_file)
    call assert_true(reader%is_valid)   
 
  end subroutine test_myreader_new

end module myreader_test
